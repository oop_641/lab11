public class App {
    private static final Crawlable Snake = null;

    public static void main(String[] args) {
        Bird bird1 = new Bird("Tweety");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();
        bird1.walk();
        bird1.run();
        Plane boeing = new Plane("Boeing", "Rosaroi");
        boeing.takeoff();
        boeing.fly();
        boeing.landing();
        Ironman iron = new Ironman("Iron man");
        iron.takeoff();
        iron.fly();
        iron.landing();
        iron.walk();
        iron.run();
        iron.eat();
        iron.sleep();
        iron.swim();
        Human lisa = new Human("Lisa");
        lisa.eat();
        lisa.sleep();
        lisa.walk();
        lisa.run();
        lisa.swim();
        Snake nake = new Snake("Nakeny");
        nake.crawl();
        nake.eat();
        nake.sleep();
        Crocodile mu = new Crocodile("Mumu");
        mu.crawl();
        mu.eat();
        mu.sleep();
        Bat bat = new Bat("Batty");
        Fish nemo = new Fish("Nemo");
        Dog mo = new Dog("Momo");
        Cat dora = new Cat("Doraemon");
        Rat je = new Rat("Jejy");

        System.out.println();

        Flyable[] flyables = { bird1, boeing, iron, bat };
        for (int i = 0; i < flyables.length; i++) {
            flyables[i].takeoff();
            flyables[i].fly();
            flyables[i].landing();
        }
        System.out.println();
        Walkable[] walkables = { bird1, iron, lisa, mo, dora, je };
        for (int i = 0; i < walkables.length; i++) {
            walkables[i].walk();
            walkables[i].run();
        }
        System.out.println();
        Swimable[] swimables = { mu, nemo, mo, dora, je };
        for (int i = 0; i < swimables.length; i++) {
            swimables[i].swim();
        }

        System.out.println();
        Crawlable[] crawlables = { nake, mu, mo };
        for (int i = 0; i < crawlables.length; i++) {
            crawlables[i].crawl();

        }
    }
}
