public abstract class Vehicle {
    private String name;
    private String engine;

    public Vehicle(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }
    public String getName() {
        return this.name;
    }

    public String getEnigne() {
        return this.engine;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEnigne(String engine) {
        this.engine = engine;
    }
}
